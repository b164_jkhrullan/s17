console.log("Hello World");

let students = []

function addStudent(name){
	students.push(name)
	console.log(name + " was added to the student's list.")
}

function countStudents() {
	console.log(`There are a total of ${students.length} students enrolled`)
}

function printStudents() {
	students.sort();
	students.forEach(function(name){
		console.log(name)
	})
}


function findStudent(name) {
	enrolled = students.filter(function(studentName) {
		return studentName.toLowerCase().startsWith(name.toLowerCase())
	})

	if (enrolled.length > 1) {
		console.log(`${enrolled} are enrollees`)
	} else if(enrolled.length == 1) {
		console.log(`${enrolled[0]} is an enrollee`)
	} else {
		console.log(`${name} is not an enrollee`)
	}
}
