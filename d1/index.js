console.log("Hello World!");


//Array and Indexes
//store multiple values in a single variable
// [] ------>Array Literals ({} ------> object Literals)


//Common Examples of Arrays 

let grades = (98.5, 91.2, 93.1, 89.0);
console.log(grades[0]);

//Alternative way to write arrays

let myTask = [
'drink html',
'eat javascript',
'bake express js'
];

//Reassigniing array values
console.log("Array before reassignment");
console.log(myTask);

myTask[0] = "Hello World;"
console.log("Array after reassignment");
console.log(myTask);

//Getting the length of an array

let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Toshiba", "Gateway", "RedFox", "Fujitsu"];
console.log(computerBrands.length);
console.log(computerBrands);

if (computerBrands.length > 5){
	console.log("We have too may suppliers.Please cooperate with the operations manager.");
}

//Access the last element of an array
let lastElementIndex = computerBrands.length -1;
console.log(computerBrands[lastElementIndex]);


//Mutator Methods

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

//push()
/* Adds an element in the end of an array AND returns array's length
	syntax:
	arrayName.push();
	*/

	console.log("Current Array")
	console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method.");
console.log(fruits);

//Add elements
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method.");
console.log(fruits);

//pop()
/*-Removes the last element in the array AND returns the removed element
	syntax:

		arrayName.pop();
		*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(fruits);

fruits.pop();
console.log(fruits);

// unshift()
/*
Adds one or more elements at the beginning of an array

Syntax:
	arrayName.unshift("elementA", "elementB") */


fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method.");
console.log(fruits);


//shift
/*
Removes an element at the beginning on an array AND returns the removed element
Syntax:
	arrayName.shift()
	*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);

//splice()

/*
Simunltatneously removes elements from a specified index number and adds elements
Syntax:
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

*/


fruits.splice(1, 2, "Lime", "Cherry");
console.log("Splice Method");
console.log(fruits);


//sort()
/*
Rearranges the array elements in alphanumeric order.

Syntax:
	arrayName.sort(;)
*/

fruits.sort();
console.log("Sort Method");
console.log(fruits);

//reverse()
/*
Reverses the order on an array elements.

Syntax:
	arrayName.reverse();
*/

fruits.reverse();
console.log("Reverse Method");
console.log(fruits);

//Non-Mutator Methods

let countries = ["US", "PH", "CAN", "SG", "TH", "FR", "DE"];

//index()
/*
	Returns the index number of the first matching element found in an array
	-if no match was found, the results -1
	- The search process will be done from the first element proceeding to the last element

	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log(`Results of indexOF method: ${firstIndex}`);

let invalidCountry = countries.indexOf("BR");
console.log(`indexOf: ${invalidCountry}`);


//lastIndexOf();
/*
Returns the index number of the LAST matching element found in an array
-search process is from last element procedding to the first element

Syntax:
	arrayName.lastIndexOf(searchValue);
*/

// let lastIndex = countries.lastIndxeOf("PH");
// console.log(`lastIndexOf method: ${lastIndex}`);

// let lastIndexStart = countries.lastIndxeOf("PH", 4);
// console.log(`lastIndexOf: ${lastIndexStart}`);

//slice

let slicedArrayA = countries.slice(2);
console.log("Slice method:");
console.log(slicedArrayA);

//slicing off elements starting from a specified index to another index
let slicedArrayB = countries.slice(2, 4);
console.log(slicedArrayB)

//starting from the last element of an array

let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);

//toString()

let stringArray = countries.toString();
console.log("toString method:");
console.log(stringArray);

//concat()

/*
Syntax:
	arrayA.concat(arrayB);
	arrayA.concat(elementA);
*/

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe sass"];
let taskArrayc = ["get git", "be node"];

// let task = taskArrayA.concat(taskArrayB);
// console.log(`concat method: ${tasks}`);
// console.log(tasks);

// combining multiple arrays
let allTasks = taskArrayA.concat(taskArrayB, taskArrayc);
console.log(allTasks);

// combing arrays with elements
let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log(combinedTasks);

// join()

let users = ["John", "Jane", "Joe", "Robert"];

console.log(users.join());
console.log(users.join(''));
console.log(users.join('_'));

// Iteration Methods

//forEach()

allTasks.forEach(function(task){
	console.log(task);
})




for (i = 0; i < allTasks.length; i++){
	console.log(allTasks[i])
}


// Using forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task)
	}
})
console.log("Result of the filtered tasks: ")
console.log(filteredTasks);


// sample with prompt
let sampleArray = ["eat", "drink"];
let data = prompt("add a data");
let firstName = prompt("add firstName");
sampleArray.unshift(data);
sampleArray.push(firstName);
console.log(sampleArray);


//map()

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return numbers * numbers
})

console.log("Map method: ")
console.log(numbers);
console.log(numberMap);




















